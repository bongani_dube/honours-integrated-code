-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 20, 2016 at 05:36 
-- Server version: 10.1.16-MariaDB
-- PHP Version: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `power_users`
--

-- --------------------------------------------------------

--
-- Table structure for table `backup_file`
--

CREATE TABLE `backup_file` (
  `id` varchar(40) NOT NULL,
  `timestamp` varchar(25) NOT NULL,
  `status` varchar(255) NOT NULL,
  `uid` varchar(255) NOT NULL,
  `filename` varchar(255) NOT NULL,
  `filesize` float NOT NULL,
  `file_location` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `backup_file`
--

INSERT INTO `backup_file` (`id`, `timestamp`, `status`, `uid`, `filename`, `filesize`, `file_location`) VALUES
('2016-10-05-22-20-41John', '2016-10-05-22-20-41', 'Restored', 'John', 'backup.zip.0', 4125210, ''),
('2016-10-05-22-20-41Matthew', '2016-10-05-22-20-41', 'Restored', 'Matthew', 'backup.zip.1', 4125210, ''),
('2016-10-05-22-20-41Peter', '2016-10-05-22-20-41', 'Restored', 'Peter', 'backup.zip.2', 4125210, ''),
('2016-10-08-16-51-48John', '2016-10-08-16-51-48', 'Restored', 'John', 'backup.zip.0', 8249770, ''),
('2016-10-08-16-51-48Matthew', '2016-10-08-16-51-48', 'Restored', 'Matthew', 'backup.zip.1', 8249770, ''),
('2016-10-08-16-51-48Peter', '2016-10-08-16-51-48', 'Restored', 'Peter', 'backup.zip.2', 8249770, ''),
('2016-10-08-17-00-20John', '2016-10-08-17-00-20', 'Downloaded', 'John', 'backup.zip.0', 16499500, ''),
('2016-10-08-17-00-20Matthew', '2016-10-08-17-00-20', 'Uploaded', 'Matthew', 'backup.zip.1', 16499500, ''),
('2016-10-08-17-00-20Peter', '2016-10-08-17-00-20', 'Downloaded', 'Peter', 'backup.zip.2', 16499500, ''),
('2016-10-13-21-42-01John', '2016-10-13-21-42-01', 'Restored', 'John', 'backup.zip.0', 33032500, ''),
('2016-10-13-21-42-01Matthew', '2016-10-13-21-42-01', 'Restored', 'Matthew', 'backup.zip.1', 33032500, ''),
('2016-10-13-21-42-01Peter', '2016-10-13-21-42-01', 'Restored', 'Peter', 'backup.zip.2', 33032500, ''),
('2016-10-18-23-13-16John', '2016-10-18-23-13-16', 'Restored', 'John', 'backup.zip.0', 4157410, ''),
('2016-10-18-23-13-16Matthew', '2016-10-18-23-13-16', 'Restored', 'Matthew', 'backup.zip.1', 4157410, ''),
('2016-10-18-23-13-16Peter', '2016-10-18-23-13-16', 'Restored', 'Peter', 'backup.zip.2', 4157410, '/storage/emulated/0/owncloud/Peter@192.168.99.100%2Fowncloud/backup.zip.2'),
('2016-10-27-17-05-19John', '2016-10-27-17-05-19', 'Uploaded', 'John', 'backup.zip.0', 9698640, ''),
('2016-10-27-17-05-19Matthew', '2016-10-27-17-05-19', 'Uploaded', 'Matthew', 'backup.zip.1', 9698640, ''),
('2016-10-27-17-05-19Peter', '2016-10-27-17-05-19', 'Waiting', 'Peter', 'backup.zip.2', 9698640, '/storage/emulated/0/owncloud/Peter@192.168.99.100%2Fowncloud/backup.zip.2');

-- --------------------------------------------------------

--
-- Table structure for table `backup_history`
--

CREATE TABLE `backup_history` (
  `timestamp` varchar(25) NOT NULL,
  `num_of_parts` int(11) NOT NULL,
  `total_size` float NOT NULL,
  `status` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `backup_history`
--

INSERT INTO `backup_history` (`timestamp`, `num_of_parts`, `total_size`, `status`) VALUES
('2016-10-05-22-20-41', 3, 12375600, 'Restored'),
('2016-10-08-16-51-48', 3, 24749300, 'Restored'),
('2016-10-08-17-00-20', 3, 49498500, 'Uploaded'),
('2016-10-13-21-42-01', 3, 99097400, 'Restored'),
('2016-10-18-23-13-16', 3, 12472200, 'Restored'),
('2016-10-27-17-05-19', 3, 29095900, 'Uploaded');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `uid` varchar(25) NOT NULL,
  `password` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`uid`, `password`) VALUES
('John', '1234'),
('Matthew', '1234'),
('Peter', '1234');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `backup_file`
--
ALTER TABLE `backup_file`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `backup_history`
--
ALTER TABLE `backup_history`
  ADD PRIMARY KEY (`timestamp`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`uid`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
