package com.owncloud.android.powerusersbackup.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.owncloud.android.R;
import com.owncloud.android.powerusersbackup.activity.PowerUserActivity;
import com.owncloud.android.powerusersbackup.asynctasks.CancelPowerUserAsyncTask;

/**
 * Created by WLLMAT011 on 2016/10/03.
 */
public class PowerUserCancelFragment extends Fragment implements CancelPowerUserAsyncTask.OnPowerUserCancelTaskListener {

    private View view;
    private Fragment mFragment;
    private Context mContext;
    private String username;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_power_user_cancel, null);

        mFragment = this;
        mContext = getContext();

        Bundle args = getArguments();
        username = args.getString(PowerUserActivity.USERNAME);

        Button button = (Button) view.findViewById(R.id.buttonCancel);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new CancelPowerUserAsyncTask(mFragment).execute(username);
            }
        });

        return view;
    }

    @Override
    public void onPowerUserCancelTaskCallback(String result) {

        if (result.equalsIgnoreCase("exception") || result.equalsIgnoreCase("unsuccessful")) {

            Toast.makeText(mContext, "OOPs! Something went wrong. Connection Problem.", Toast.LENGTH_LONG).show();
        } else {

            //Cancel successful
            Toast.makeText(mContext, "You are no longer a power user.", Toast.LENGTH_LONG).show();
        }

        // Close the activity after cancel sign up
        getActivity().finish();

    }
}
