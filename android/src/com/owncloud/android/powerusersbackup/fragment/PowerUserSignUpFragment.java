package com.owncloud.android.powerusersbackup.fragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.owncloud.android.R;
import com.owncloud.android.powerusersbackup.activity.PowerUserActivity;
import com.owncloud.android.powerusersbackup.asynctasks.SignUpPowerUserAsyncTask;

/**
 * Created by WLLMAT011 on 2016/10/03.
 */
public class PowerUserSignUpFragment extends Fragment implements SignUpPowerUserAsyncTask.OnPowerUserSignUpTaskListener{

    private View view;
    private Button buttonInsert;
    private ProgressDialog progressDialogLoading;

    private Activity mActivity;
    private Context mContext;
    private Fragment mFragment;

    private String username;
    private String password;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_power_user_signup, null);

        Bundle args = getArguments();
        username = args.getString(PowerUserActivity.USERNAME);
        password = args.getString(PowerUserActivity.PASSWORD);

        mActivity = getActivity();
        mContext = getContext();
        mFragment = this;

        // Triggers when LOGIN Button clicked
        buttonInsert = (Button) view.findViewById(R.id.buttonInsert);
        buttonInsert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // Initialize  SignupPowerUserTask() class with uid and password
                progressDialogLoading = new ProgressDialog(mContext);
                progressDialogLoading.setMessage("\tSigning Up...");
                progressDialogLoading.setCancelable(false);
                progressDialogLoading.show();

                new SignUpPowerUserAsyncTask(mFragment).execute(username, password);
            }
        });

        return view;
    }

    @Override
    public void onPowerUserSignUpTaskCallback(String result) {

        progressDialogLoading.dismiss();

        if(!result.equals("null"))
        {
            //Sign up successful
            Toast.makeText(mContext, "You are now a power user", Toast.LENGTH_LONG).show();

        }else if (result.equals("null")){

            // If username and password does not match display a error message
            Toast.makeText(mContext, "You are already a Power User.", Toast.LENGTH_LONG).show();

        } else if (result.equalsIgnoreCase("exception") || result.equalsIgnoreCase("unsuccessful")) {

            Toast.makeText(mContext, "OOPs! Something went wrong. Connection Problem.", Toast.LENGTH_LONG).show();
        }

        // Close the activity after sign up
        getActivity().finish();
    }
}
