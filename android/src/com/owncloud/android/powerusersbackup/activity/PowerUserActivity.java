package com.owncloud.android.powerusersbackup.activity;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.owncloud.android.R;
import com.owncloud.android.powerusersbackup.asynctasks.CheckPowerUserAsyncTask;
import com.owncloud.android.powerusersbackup.fragment.PowerUserCancelFragment;
import com.owncloud.android.powerusersbackup.fragment.PowerUserSignUpFragment;
import com.owncloud.android.ui.activity.FileActivity;

public class PowerUserActivity extends AppCompatActivity implements CheckPowerUserAsyncTask.OnCheckPowerUserTaskListener {

    // CONNECTION_TIMEOUT and READ_TIMEOUT are in milliseconds

    public static final String USERNAME = "com.owncloud.android.p2p.USERNAME";
    public static final String PASSWORD = "com.owncloud.android.p2p.PASSWORD";

    private ProgressBar progressBar;
    private String username;
    private String password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_power_user);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Power User");

        progressBar = (ProgressBar) findViewById(R.id.marker_progress);

        username = getIntent().getExtras().getString(FileActivity.EXTRA_USERNAME);
        password = getIntent().getExtras().getString(FileActivity.EXTRA_PASSWORD);

        new CheckPowerUserAsyncTask(this).execute(username);

    }

    @Override
    public void onCheckPowerUserTaskCallback(String result) {

        progressBar.setVisibility(View.GONE);

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        Fragment fragment = null;

        if(result != null && !result.equals("exception"))
        {
            // User is already a power user
            fragment = new PowerUserCancelFragment();
            Bundle args = new Bundle();
            args.putString(USERNAME, username);
            args.putString(PASSWORD, password);
            fragment.setArguments(args);

        } else if (result == null){

            // User is not a power user - launch sign-up screen
            fragment = new PowerUserSignUpFragment();
            Bundle args = new Bundle();
            args.putString(USERNAME, username);
            args.putString(PASSWORD, password);
            fragment.setArguments(args);

        } else if (result.equalsIgnoreCase("exception")) {

            Toast.makeText(getApplicationContext(), "Error connecting to Power User database.", Toast.LENGTH_LONG).show();
        }

        // Replace whatever is in the fragment_container view with this fragment,
        // and add the transaction to the back stack so the user can navigate back
        transaction.replace(R.id.fragment_container, fragment);
        //transaction.addToBackStack(null);

        // Commit the transaction
        transaction.commit();

    }
}
