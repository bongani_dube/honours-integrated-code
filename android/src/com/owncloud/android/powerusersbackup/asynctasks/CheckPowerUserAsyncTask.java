package com.owncloud.android.powerusersbackup.asynctasks;

import android.accounts.Account;
import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;

import com.owncloud.android.authentication.AccountUtils;
import com.owncloud.android.utils.DisplayUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by Matthew on 9/18/2016.
 *
 *  Async task that checks if owncloud user is registered as a power user
 *  if not launch a pop-up dialog prompting sign up
 *
 */
public class CheckPowerUserAsyncTask extends AsyncTask<String, String, String> {

    public static final int CONNECTION_TIMEOUT = 10000;
    public static final int READ_TIMEOUT = 15000;

    private Context mContext;
    private final WeakReference<OnCheckPowerUserTaskListener> mListener;

    private HttpURLConnection conn;
    private URL url = null;
    private String urlAddress;
    private final String apiPath = "/powerusers-api.php/users";

    private String username;

    public CheckPowerUserAsyncTask(Activity activity){
        mContext = activity.getApplicationContext();
        mListener = new WeakReference<OnCheckPowerUserTaskListener>((OnCheckPowerUserTaskListener) activity);

        Account currentAccount = AccountUtils.getCurrentOwnCloudAccount(mContext);
        String accountURL = DisplayUtils.convertIdn(currentAccount.name, false);
        urlAddress = "http://" + accountURL.substring(accountURL.indexOf('@')+1);
    }

    @Override
    protected String doInBackground(String... params) {

        username = params[0];

        try {

            // Enter URL address where your php file resides
            url = new URL(urlAddress + apiPath + "/" + username);

        } catch (MalformedURLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return "exception";
        }
        try {
            // Setup HttpURLConnection class to send and receive data from php and mysql
            conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(READ_TIMEOUT);
            conn.setConnectTimeout(CONNECTION_TIMEOUT);
            conn.setRequestMethod("GET");

            conn.connect();

        } catch (IOException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
            return "exception";
        }

        try {

            int response_code = conn.getResponseCode();

            // Check if successful connection made
            if (response_code == HttpURLConnection.HTTP_OK) {

                // Read data sent from server
                InputStream inputStream = conn.getInputStream();
                StringBuffer buffer = new StringBuffer();
                if(inputStream == null){
                    return null;
                }
                BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));

                String line;
                while((line = reader.readLine()) != null){
                    // Newline isn't necessary for JSON, but makes debugging easier
                    buffer.append(line + "\n");
                }

                if(buffer.length() == 0){
                    return null;
                }

                // Pass data to onPostExecute method
                return( buffer.toString());

            }else {

                return null;
            }

        } catch (IOException e) {
            e.printStackTrace();
            return "exception";
        } finally {
            conn.disconnect();
        }
    }

    @Override
    protected void onPostExecute(String result) {

        OnCheckPowerUserTaskListener listener = mListener.get();
        if (listener!= null)
        {
            listener.onCheckPowerUserTaskCallback(result);
        }

    }

    /*
     * Interface to retrieve data from recognition task
     */
    public interface OnCheckPowerUserTaskListener{

        void onCheckPowerUserTaskCallback(String result);
    }
}
