package com.owncloud.android.powerusersbackup.asynctasks;

import android.accounts.Account;
import android.content.Context;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;

import com.owncloud.android.authentication.AccountUtils;
import com.owncloud.android.utils.DisplayUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by Matthew on 10/4/2016.
 */
public class CancelPowerUserAsyncTask extends AsyncTask<String, String, String> {

    private static final int CONNECTION_TIMEOUT = 10000;
    private static final int READ_TIMEOUT = 15000;

    private Context mContext;
    private final WeakReference<OnPowerUserCancelTaskListener> mListener;

    private HttpURLConnection conn;
    private URL url = null;
    private String urlAddress;
    private final String apiPath = "/powerusers-api.php/users";

    private String username;

    public CancelPowerUserAsyncTask(Fragment fragment){
        mContext = fragment.getContext();
        mListener = new WeakReference<OnPowerUserCancelTaskListener>((OnPowerUserCancelTaskListener) fragment);

        Account currentAccount = AccountUtils.getCurrentOwnCloudAccount(mContext);
        String accountURL = DisplayUtils.convertIdn(currentAccount.name, false);
        urlAddress = "http://" + accountURL.substring(accountURL.indexOf('@')+1);
    }

    @Override
    protected String doInBackground(String... params) {

        username = params[0];

        try {

            // Enter URL address where your php file resides
            url = new URL(urlAddress + apiPath + "/" + username);

        } catch (MalformedURLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return "exception";
        }
        try {
            // Setup HttpURLConnection class to send and receive data from php and mysql
            conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(READ_TIMEOUT);
            conn.setConnectTimeout(CONNECTION_TIMEOUT);
            conn.setRequestMethod("DELETE");

            conn.connect();

        } catch (IOException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
            return "exception";
        }

        try {

            int response_code = conn.getResponseCode();

            // Check if successful connection made
            if (response_code == HttpURLConnection.HTTP_OK) {

                // Read data sent from server
                InputStream inputStream = conn.getInputStream();
                StringBuffer buffer = new StringBuffer();
                if(inputStream == null){
                    return null;
                }
                BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));

                String line;
                while((line = reader.readLine()) != null){
                    // Newline isn't necessary for JSON, but makes debugging easier
                    buffer.append(line + "\n");
                }

                if(buffer.length() == 0){
                    return null;
                }

                // Pass data to onPostExecute method
                return( buffer.toString());

            }else {

                return null;
            }

        } catch (IOException e) {
            e.printStackTrace();
            return "exception";
        } finally {
            conn.disconnect();
        }
    }

    @Override
    protected void onPostExecute(String result) {
        if (result!= null)
        {
            OnPowerUserCancelTaskListener listener = mListener.get();
            if (listener!= null)
            {
                listener.onPowerUserCancelTaskCallback(result);
            }
        }
    }

    public interface OnPowerUserCancelTaskListener{
        void onPowerUserCancelTaskCallback(String result);
    }

}
