package com.owncloud.android.powerusersbackup.asynctasks;

import android.accounts.Account;
import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.widget.Toast;

import com.owncloud.android.authentication.AccountUtils;
import com.owncloud.android.utils.DisplayUtils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by Matthew on 10/9/2016.
 */
public class UpdateBackupStatusAsyncTask extends AsyncTask<String, String, String> {

    private static final int CONNECTION_TIMEOUT = 10000;
    private static final int READ_TIMEOUT = 15000;

    private Context mContext;

    private HttpURLConnection conn;
    private URL url = null;
    private String urlAddress;
    private final String apiPath = "/powerusers-api.php/backup_file";
    private String backupFileId;
    private String status;

    public UpdateBackupStatusAsyncTask(Activity activity){
        mContext = activity.getApplicationContext();

        Account currentAccount = AccountUtils.getCurrentOwnCloudAccount(mContext);
        String accountURL = DisplayUtils.convertIdn(currentAccount.name, false);
        urlAddress = "http://" + accountURL.substring(accountURL.indexOf('@')+1);
    }

    @Override
    protected String doInBackground(String... params) {

        backupFileId = params[0];
        status = params[1];

        try {

            // Enter URL address where your php file resides
            url = new URL(urlAddress + apiPath + "/" + backupFileId);

        } catch (MalformedURLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return "exception";
        }
        try {
            // Setup HttpURLConnection class to send and receive data from php and mysql
            conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(READ_TIMEOUT);
            conn.setConnectTimeout(CONNECTION_TIMEOUT);
            conn.setRequestMethod("PUT");

            // setDoInput and setDoOutput method depict handling of both send and receive
            conn.setDoInput(true);
            conn.setDoOutput(true);

            // Append parameters to URL
            Uri.Builder builder = new Uri.Builder()
                    .appendQueryParameter("status", status);
            String query = builder.build().getEncodedQuery();

            // Open connection for sending data
            OutputStream os = conn.getOutputStream();
            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(os, "UTF-8"));
            writer.write(query);
            writer.flush();
            writer.close();
            os.close();
            conn.connect();

        } catch (IOException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
            return "exception";
        }

        try {

            int response_code = conn.getResponseCode();

            // Check if successful connection made
            if (response_code == HttpURLConnection.HTTP_OK) {

                // Read data sent from server
                InputStream input = conn.getInputStream();
                BufferedReader reader = new BufferedReader(new InputStreamReader(input));
                StringBuilder result = new StringBuilder();
                String line;

                while ((line = reader.readLine()) != null) {
                    result.append(line);
                }

                // Pass data to onPostExecute method
                return(result.toString());

            } else{
                return null;
            }

        } catch (IOException e) {
            e.printStackTrace();
            return "exception";
        } finally {
            conn.disconnect();
        }
    }

    @Override
    protected void onPostExecute(String result) {
        if(result == null || result.equals("exception")){
            Toast.makeText(mContext, "Error connecting to database.", Toast.LENGTH_LONG).show();
        }
        else {
            Toast.makeText(mContext, "Backup file details updated to " + status, Toast.LENGTH_LONG).show();
        }
    }
}
