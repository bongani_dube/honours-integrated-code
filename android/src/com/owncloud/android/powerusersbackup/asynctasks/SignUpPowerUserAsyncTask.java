package com.owncloud.android.powerusersbackup.asynctasks;

import android.accounts.Account;
import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;

import com.owncloud.android.authentication.AccountUtils;
import com.owncloud.android.utils.DisplayUtils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by Matthew on 9/18/2016.
 */
public class SignUpPowerUserAsyncTask extends AsyncTask<String, String, String> {

    private static final int CONNECTION_TIMEOUT = 10000;
    private static final int READ_TIMEOUT = 15000;

    private Context mContext;
    private final WeakReference<OnPowerUserSignUpTaskListener> mListener;

    private HttpURLConnection conn;
    private URL url = null;
    private String urlAddress;
    private final String apiPath = "/powerusers-api.php/users";

    public SignUpPowerUserAsyncTask(Fragment fragment) {
        mContext = fragment.getContext();
        mListener = new WeakReference<OnPowerUserSignUpTaskListener>((OnPowerUserSignUpTaskListener) fragment);

        Account currentAccount = AccountUtils.getCurrentOwnCloudAccount(mContext);
        String accountURL = DisplayUtils.convertIdn(currentAccount.name, false);
        urlAddress = "http://" + accountURL.substring(accountURL.indexOf('@')+1);
    }

    @Override
    protected String doInBackground(String... params) {
        try {

            // Enter URL address where your php file resides
            url = new URL(urlAddress + apiPath);

        } catch (MalformedURLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return "exception";
        }
        try {
            // Setup HttpURLConnection class to send and receive data from php and mysql
            conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(READ_TIMEOUT);
            conn.setConnectTimeout(CONNECTION_TIMEOUT);
            conn.setRequestMethod("POST");

            // setDoInput and setDoOutput method depict handling of both send and receive
            conn.setDoInput(true);
            conn.setDoOutput(true);

            // Append parameters to URL
            Uri.Builder builder = new Uri.Builder()
                    .appendQueryParameter("uid", params[0])
                    .appendQueryParameter("password", params[1]);
            String query = builder.build().getEncodedQuery();

            // Open connection for sending data
            OutputStream os = conn.getOutputStream();
            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(os, "UTF-8"));
            writer.write(query);
            writer.flush();
            writer.close();
            os.close();
            conn.connect();

        } catch (IOException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
            return "exception";
        }

        try {

            int response_code = conn.getResponseCode();

            // Check if successful connection made
            if (response_code == HttpURLConnection.HTTP_OK) {

                // Read data sent from server
                InputStream input = conn.getInputStream();
                BufferedReader reader = new BufferedReader(new InputStreamReader(input));
                StringBuilder result = new StringBuilder();
                String line;

                while ((line = reader.readLine()) != null) {
                    result.append(line);
                }

                // Pass data to onPostExecute method
                return(result.toString());

            }else{

                return("unsuccessful");
            }

        } catch (IOException e) {
            e.printStackTrace();
            return "exception";
        } finally {
            conn.disconnect();
        }


    }

    @Override
    protected void onPostExecute(String result) {

        if (result!= null)
        {
            OnPowerUserSignUpTaskListener listener = mListener.get();
            if (listener!= null)
            {
                listener.onPowerUserSignUpTaskCallback(result);
            }
        }
    }

    /*
     * Interface to retrieve data from recognition task
     */
    public interface OnPowerUserSignUpTaskListener{

        void onPowerUserSignUpTaskCallback(String result);
    }

}
