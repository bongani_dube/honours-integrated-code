# This is the Android client for ownCloud with the Power User Registration and Backup Process

The app performs file synchronization with an ownCloud server and also handles the power user registration and backup process.

## Project Setup

Make sure you read [SETUP.md](https://github.com/owncloud/android/blob/master/SETUP.md) when you start working on this project.

## Documentation

For information on extending or maintaining the code base refer to the documentation included with this archive - `globalcloudlet/distributed-backup-docs/site/index.html`.

## Running Tests

The tests for the power users registration and backup process are included under the powerusersbackup-tests/ directory. The tests for this project are built using maven. Maven is needed to run tests. 

To install Maven:
1. Download maven - https://maven.apache.org/download.cgi
2. Follow the installation instructions - https://maven.apache.org/install.html

Dependencies: JUnit

### Compile, run unit tests and package project into a jar file:

	mvn package

### Run unit tests:

	mvn test
