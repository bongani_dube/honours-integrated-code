module DiasporaOwncloud
  class Engine < ::Rails::Engine
    
    initializer :append_migrations do |app|
      unless app.root.to_s.match root.to_s
        config.paths["db/migrate"].expanded.each do |expanded_path|
          app.config.paths["db/migrate"] << expanded_path
        end
      end
    end
    
    initializer 'DiasporaOwncloud', before: :load_config_initializers do
      Rails.application.config.i18n.load_path += Dir["#{config.root}/config/locales/**/*.yml"]        
    end
    
    config.generators do |g|
      g.test_framework :rspec
    end
  
  end
end
