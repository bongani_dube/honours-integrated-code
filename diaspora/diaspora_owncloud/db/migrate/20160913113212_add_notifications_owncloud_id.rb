class AddNotificationsOwncloudId < ActiveRecord::Migration
  def change
    add_column :notifications, :owncloud_share_id, :integer
    add_column :notifications, :owncloud_sync_id, :integer
    add_column :notifications, :owncloud_link, :string
    add_column :notifications, :owncloud_file, :string
    add_column :notifications, :owncloud_server, :string
  end
end
