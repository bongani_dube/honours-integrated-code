class AddSessionsTable < ActiveRecord::Migration
  def change
    create_table :sessions do |t|
      t.string :session_id, :null => false
      t.text :data
      t.timestamps
    end
    
    add_index :sessions, :session_id, :length => { :session_id => 255 }, :unique => true
    add_index :sessions, :updated_at, :length => { :updated_at => 255 }
  end
end
