$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "diaspora_owncloud/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "diaspora_owncloud"
  s.version     = DiasporaOwncloud::VERSION
  s.authors     = ["Takunda Chirema"]
  s.email       = ["chrtak003@myuct.ac.za"]
  s.homepage    = "https://za.linkedin.com/in/takunda-chirema-0755b959"
  s.summary     = "Diaspora Owncloud Integration"
  s.description = "Diaspora Owncloud Integration"
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.rdoc"]
  s.test_files = Dir["test/**/*"]

  s.add_dependency "rails", "~> 4.2.7.1"
  
  s.add_dependency "mechanize"
  
  s.add_dependency 'fcfinder'

  s.add_dependency 'net_dav'

  s.add_dependency "koala", "~> 2.2"

  s.add_dependency 'activerecord-session_store'

  s.add_development_dependency "sqlite3"
end