class DiasporaOwncloudSessionsController < ::SessionsController
  
  layout proc { request.format == :mobile ? "diaspora_owncloud/application" : 
      "diaspora_owncloud/with_header" }
  
  after_filter :reset_authentication_token, :only => [:create]
  before_filter :reset_authentication_token, :only => [:destroy]
  include OwncloudHelper
  
  def sign_in(resource_name, resource)
    set_owncloud_cookies(resource_params[:username],resource_params[:password])
    set_owncloud_notifications(resource_params[:username],resource_params[:password])
  end
  
  def reset_authentication_token
    current_user.reset_authentication_token! unless current_user.nil?
  end
  
end
