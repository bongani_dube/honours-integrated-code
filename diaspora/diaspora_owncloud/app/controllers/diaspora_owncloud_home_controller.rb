class DiasporaOwncloudHomeController < ::HomeController
  
  layout proc { request.format == :mobile ? "diaspora_owncloud/application" : 
      "diaspora_owncloud/with_header" }
  
  def show
    partial_dir = Rails.root.join("diaspora_owncloud/app", "diaspora_owncloud/views", "diaspora_owncloud/home")
    if user_signed_in?
      redirect_to stream_path
    elsif request.format == :mobile
      if partial_dir.join("_show.mobile.haml").exist? ||
         partial_dir.join("_show.mobile.erb").exist? ||
         partial_dir.join("_show.haml").exist?
        puts "===== rendering show"
        render :show
      else
        puts "===== redirect to session"
        redirect_to user_session_path
      end
    elsif partial_dir.join("_show.html.haml").exist? ||
          partial_dir.join("_show.html.erb").exist? ||
          partial_dir.join("_show.haml").exist?
      puts "===== rendering show"
      render :show
    elsif User.count > 1 && Role.where(name: "admin").any?
      puts "===== rendering default"
      render :default
    else
      puts "===== rendering redirect to podmin"
      redirect_to podmin_path
    end
  end

  def podmin
    render :podmin
  end

  def toggle_mobile
    session[:mobile_view] = session[:mobile_view].nil? ? true : !session[:mobile_view]

    redirect_to :back
  end

  def force_mobile
    session[:mobile_view] = true

    redirect_to stream_path
  end

end
