module OwncloudHelper
  
  def set_session_variables
    require 'uri'
    
    diaspora_session_variables
    
    owncloud_session_variables
   
  end
  
  def diaspora_session_variables
    require 'uri'
    
    #get the handle for the application from diaspora_owncloud
    diaspora_yml = YAML.load_file(Rails.root.join("config", "diaspora.yml"))
    url = diaspora_yml['configuration']['environment']['url']
    uri = URI(url)
    
    session[:diaspora_host] = uri.host
    
    if (url[-1, 1].eql? "/")
      session[:diaspora_url] = url
    else
      session[:diaspora_url] = url+"/"
    end
    
    
    if (uri.port != nil)
      session[:diaspora_handle] = uri.host+":"+uri.port.to_s
    else
      session[:diaspora_handle] = uri.host
    end
    
    puts "%%%%%%% diaspora handle "+session[:diaspora_handle]
  end
  
  def owncloud_session_variables
    require 'uri'
    
    owncloud_yml = YAML.load_file(Rails.root.join("diaspora_owncloud/config", "owncloud.yml"))
    url = owncloud_yml['configuration']['environment']['url']
    
    sync = owncloud_yml['configuration']['environment']['sync']
    global_url = owncloud_yml['configuration']['environment']['global_url']
    
    # share photos to owncloud on upload
    photo_share = owncloud_yml['configuration']['environment']['photo_share']
    
    if (url[-1, 1].eql? "/")
      owncloud_handle = url
    else
      owncloud_handle = url+"/"
    end
    
    if (sync)
      puts "%%%%%%% sync is true"
      if (global_url[-1, 1].eql? "/")
        global_url = global_url
      else
        global_url = global_url+"/"
      end
      
      session[:owncloud_global_url] = global_url
    end
    
    session[:sync] = sync
    session[:owncloud_photo_share] = photo_share
    
    puts "%%%%%%% owncloud handle "+owncloud_handle
    session[:owncloud_handle] = owncloud_handle
    
    owncloud_username = (owncloud_yml['configuration']['environment']['username']).to_s
    owncloud_password = (owncloud_yml['configuration']['environment']['password']).to_s
    
    session[:owncloud_username] = owncloud_username
    session[:owncloud_password] = owncloud_password
    
    puts "%%%%%%% owncloud username password "+owncloud_username+" "+owncloud_password
    
  end
  
  
  def set_owncloud_cookies(username,password)
    
    require 'rubygems'
    require 'mechanize'
    require 'net/dav'
    
    puts "%%%%%%% in owncloud sessions "+username
    
    set_session_variables

    @agent = Mechanize.new
    
    begin
      
      @agent.get(session[:owncloud_handle]+"index.php/login") do | home_page |
        login_form = home_page.form_with(:name => "login")
        login_form.checkbox_with(:name => 'remember_login').check
        login_form.user = username
        login_form.password = password
        @agent.submit(login_form)

        #owncloud_cookies will be used in the owncloud controller show action
        session[:owncloud_cookies] = (@agent.cookie_jar.store.map {|i|  i}).to_yaml

      end

      #dav will be used in the owncloud controller webdav action

      $dav = Net::DAV.new(session[:owncloud_handle]+"remote.php/webdav/", :curl => true)
      $dav.verify_server = false

      #Dont put the env. Its ruining everything!
      $dav.credentials(username,password)

      session[:dav] = ($dav).to_yaml
      
      return
    rescue
      puts "Owncloud server is off"
      
      return
    end
    
  end
  
  def set_owncloud_notifications(username,password)
    set_owncloud_share_notifications(username,password)
    puts "before sync "+session[:sync].inspect
    if (session[:sync])
      set_owncloud_sync_notifications(username,password)
    end
  end
  
  
  def set_owncloud_share_notifications(username,password)
    puts "in share 1"
    
    require 'net/http'
    require "uri"
  
    uri = URI.parse(session[:owncloud_handle]+"ocs/v1.php/apps/files_sharing/api/v1/shares")
    params = {'shared_with_me' => true}
    uri.query = URI.encode_www_form(params)
    puts "in share 2"
    
    begin
      http = Net::HTTP.new(uri.host, uri.port)
      request = Net::HTTP::Get.new(uri.request_uri)
      request.basic_auth(username, password)
      request.set_form_data(params)
      response = http.request(request)
    
      data = Hash.from_xml(response.body)
      
      statuscode = data["ocs"]["meta"]["statuscode"]
      share_data = data["ocs"]["data"]

      puts " $$$$$$$$$$ shared data "+data.inspect
    rescue
      return
    end
    
    
    if (statuscode.eql? "100") || (statuscode.eql? "102")
      
      if share_data != nil
        share_data.each do |key, element|
          #get the handle for the application
          diaspora_handle = session[:diaspora_handle]
          diaspora_host = session[:diaspora_host]
          
          puts diaspora_handle

          #check if the notification is already in there
          if Notification.where(:owncloud_share_id => element['id']).blank?
            puts '--- person to take '+((element['uid_owner'].downcase)+"@"+diaspora_handle)
            @person = Person.find_by_diaspora_handle((element['uid_owner'].downcase)+"@"+diaspora_handle)

            if (@person == nil)
                @person = Person.find_by_diaspora_handle(element['uid_owner']+"@localhost")
            end
            
            if (@person == nil)
                @person = Person.find_by_diaspora_handle((element['uid_owner'].downcase)+"@"+diaspora_host)
            end

            @opts = {:target_id => "",
              :target_type => "Person",
              :type => 'Notifications::OwncloudSharing',
              :actors => [@person],
              :owncloud_link => session[:owncloud_handle]+"index.php/apps/files/?dir=/&view=sharingin", 
              :owncloud_share_id => element['id'],
              :owncloud_file => element['file_target'].gsub("/", ""),
              :recipient_id => current_user.id}
            @note = Notification.new(@opts)
            @note.save

          end

        end

      end
    
    end
   
  end
  
  def set_owncloud_sync_notifications(username,password)
    
    puts " $$$$$$ in sync notifications"
    
    require 'net/http'
    require "uri"
  
    uri = URI.parse(session[:owncloud_global_url]+"api.php/oc_syncactivity?filter=user_id,eq,"+username+"&filter=read,eq,no")
    
    begin
      http = Net::HTTP.new(uri.host, uri.port)
      request = Net::HTTP::Get.new(uri.request_uri)
      request.basic_auth(username,password)
      response = http.request(request)
    
      data = eval(response.body)
      
    rescue
      return
    end
    
    
    if (data != nil)
      puts " $$$$$$$$$$ sync data "+data[:oc_syncactivity].inspect
      columns = data[:oc_syncactivity][:columns]
      
      #array = ['a', 'b', 'c']
      #hash = Hash[array.map.with_index.to_a] # => {"a"=>0, "b"=>1, "c"=>2}
      columns_hash = Hash[columns.map.with_index.to_a]
      
      data[:oc_syncactivity][:records].each { |record|
        
        #Only files in both masi and ocean view have been synchronized
        if (!record[columns_hash["masi_and_ocean"]].nil? && !record[columns_hash["masi_and_ocean"]].empty?)
          server = "Masiphumelele and Oceanview"
          file = record[columns_hash["masi_and_ocean"]].split("/").last

          #check if the notification is already in there
          if Notification.where(:owncloud_sync_id => record[columns_hash["id"]]).blank?
            puts '--- sync notification: file is '+file

            @person = current_user.person

            @opts = {:target_id => "",
              :target_type => "Person",
              :type => 'Notifications::OwncloudSynchronization',
              :actors => [@person],
              :owncloud_link => session[:owncloud_handle]+"index.php/apps/mysyncapp/", 
              :owncloud_sync_id => record[columns_hash["id"]],
              :owncloud_file => file,
              :owncloud_server => server,
              :recipient_id => current_user.id}
            @note = Notification.new(@opts)
            @note.save

          end
          
        end
        
      }
      
    end
   
  end
  
  def photo_share(owncloud_photo)
    puts " *********** owncloud share photo "+owncloud_photo.inspect
    
    #check if diaspora folder is exists
    begin
      @use_dav = YAML.load(session[:dav])
      
      if (@use_dav.exists?('./Diaspora'))
        puts " %%%%%% Diaspora folder exists"
      else
        @use_dav.mkdir('./Diaspora')

        if (@use_dav.exists?('./Diaspora'))
          puts " %%%%%% Diaspora folder created"
        else
          puts " %%%%%% Diaspora folder does not exist"
        end

      end

      #check if image is already there
      if (@use_dav.exists?('./Diaspora/'+owncloud_photo["name"]))
        puts " %%%%%% Diaspora image exists"
      else

        #upload image to owncloud
        File.open(Rails.root.join(
            'public', 'uploads', 'images', owncloud_photo["remote_photo_name"]
              ), 'rb'){|file|

          @use_dav.put('./Diaspora/'+owncloud_photo["name"],file, File.size(file))

        }

        if (@use_dav.exists?('./Diaspora/'+owncloud_photo["name"]))
          puts " %%%%%% Diaspora image created"
        else
          puts " %%%%%% Diaspora image does not exist"
        end

      end
    
    rescue
      puts "owncloud server is down "
      return
    end
    
  end
end
