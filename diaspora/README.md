# diaspora* - Owncloud
### A privacy-aware, distributed, open source social network

**master:** [![Build Status master](https://secure.travis-ci.org/diaspora/diaspora.svg?branch=master)](http://travis-ci.org/diaspora/diaspora)
**stable:** [![Build Status stable](https://secure.travis-ci.org/diaspora/diaspora.svg?branch=stable)](http://travis-ci.org/diaspora/diaspora)
[![Coverage Status stable](https://coveralls.io/repos/github/diaspora/diaspora/badge.svg?branch=stable)](https://coveralls.io/github/diaspora/diaspora?branch=stable)|
**develop:** [![Build Status develop](https://secure.travis-ci.org/diaspora/diaspora.svg?branch=develop)](http://travis-ci.org/diaspora/diaspora)
[![Coverage Status develop](https://coveralls.io/repos/github/diaspora/diaspora/badge.svg?branch=develop)](https://coveralls.io/github/diaspora/diaspora?branch=develop) |
[![Dependency Status](https://gemnasium.com/badges/github.com/diaspora/diaspora.svg)](https://gemnasium.com/diaspora/diaspora)
[![Code Climate](https://codeclimate.com/github/diaspora/diaspora/badges/gpa.svg)](https://codeclimate.com/github/diaspora/diaspora)

[Project site](https://diasporafoundation.org) |
[Wiki](https://wiki.diasporafoundation.org) |
[Bugtracker](https://github.com/diaspora/diaspora/issues) |
[Discussions](https://www.loomio.org/groups/194) |
[Mailing lists](https://wiki.diasporafoundation.org/How_We_Communicate#Mailing_Lists) |
[License](/COPYRIGHT) |
[Authors](https://github.com/diaspora/diaspora/contributors)

## Note

This is a guide for the plugin built for the integration of Diaspora* and [ownCloud](https://owncloud.org/). To get the official repository for Diaspora* refer to their [github account](https://github.com/diaspora/diaspora). The name of this plugin in diaspora_owncloud and was built on the [engines](http://guides.rubyonrails.org/engines.html) architecture for ruby on rails.

## Installation

The plugin works with diaspora and so diaspora must already be [installed](https://wiki.diasporafoundation.org/Installation) and working. The plugin is a full engine and not mountable engine. This means that the routes of the plugin are not namespaced or separate from the main application. More information can be found [here](https://www.bignerdranch.com/blog/intro-to-rails-engines/). The installation will involve changing the routes file for the main application.

###Step 1

Get the folder for the plugin by downloading from this repository and put it in the diaspora folder as is done here (same folder with Gemfile).

###Step 2

To use the plugin, it must be included in the Gemfile like all other gems. Add the following lines to the Gemfile:


```
#!ruby

gem 'diaspora_owncloud', path: 'diaspora_owncloud'

gem 'fcfinder'

gem 'net_dav'

gem "koala", "~> 2.2"

gem 'activerecord-session_store'

```


The first line is the plugin. The following lines are gems needed by the plugin.

###Step 3

In the file diaspora/configuration/initializers/session_store.rb comment out the line that is there and replace it with this one:


```
#!ruby

Diaspora::Application.config.session_store :active_record_store, :key => '_diaspora_session', httponly: false
```


This is for using the active record sessions table to store session information instead of cookies. This gives the plugin more space to store session variables.

###Step 4

In the file diaspora/configuration/logging.rb put in this line after the class initialization:

```
#!ruby

Logging.logger.send :include, ActiveRecord::SessionStore::Extension::LoggerSilencer

```

It is also for the session variables as explained in step 3.

###Step 5

In the file diaspora/configuration/routes.rb comment out the following lines:

  
```
#!ruby

  get "activity" => "streams#activity", :as => "activity_stream"
  get "stream" => "streams#multi", :as => "stream"
  get "public" => "streams#public", :as => "public_stream"
  get "followed_tags" => "streams#followed_tags", :as =>  "followed_tags_stream"
  get "mentions" => "streams#mentioned", :as => "mentioned_stream"
  get "liked" => "streams#liked", :as => "liked_stream"
  get "commented" => "streams#commented", :as => "commented_stream"
  get "aspects" => "streams#aspects", :as => "aspects_stream"

```


And

  
```
#!ruby

devise_for :users, controllers: {sessions: "sessions",
    :omniauth_callbacks => "omniauth_callbacks"}, 
    skip: :registration
    
  devise_scope :user do
    get "/users/sign_up" => "registrations#new",    :as => :new_user_registration
    post "/users"        => "registrations#create", :as => :user_registration
  end

```


And

  
```
#!ruby

  get 'mobile/toggle', :to => 'home#toggle_mobile', :as => 'toggle_mobile'
  get "/m", to: "home#force_mobile", as: "force_mobile"

```

They are being override by the plugin.

###Step 6

Run bundle install

###Step 7

In the file diaspora/diaspora_owncloud/config/owncloud.yml specify the link for the owncloud application. This can be on the same server or on another remote server. For example:

```
#!ruby
url: "http://137.158.59.208/owncloud/"

```

Also specify the admin username and password as shown below:

```
#!ruby

## Set the password and username for the owncloud administrator
username: takunda
password: 123456

```

###Step 8

Run the migrations. There are tables that are edited by the plugin.

###Step 8

To have a link from ownCloud to diaspora insert this html tag in the file

ownCloud/core/templates/layout.user.php

```
#!html

<a style="font-size:20px;font-weight:600;color:#fff;margin-left:150px;padding; top:50px;" href="Diaspora_Url/stream" class="navbar-brand" data-stream-title="Stream">
    diaspora*
</a>

```

###Step 10

Start Diaspora ./script/server

## Questions?

Have a look at our FAQs [for users](https://wiki.diasporafoundation.org/FAQ_for_users), [for pod administrators](https://wiki.diasporafoundation.org/FAQ_for_pod_maintainers) or [for developers](https://wiki.diasporafoundation.org/FAQ_for_developers).

Still haven't found an answer? Talk to us! Read [how we communicate](https://wiki.diasporafoundation.org/How_we_communicate). We're here to answer all your questions.

## Contribute

To keep diaspora*  growing and improving we need all help we can get. Whether you can contribute [code](https://wiki.diasporafoundation.org/Getting_started_with_contributing), [ideas](https://wiki.diasporafoundation.org/How_we_communicate#Loomio), [translations](https://wiki.diasporafoundation.org/Contribute_translations), [bug reports](https://wiki.diasporafoundation.org/How_to_report_a_bug) or simply extend the community as a [helpful user](https://wiki.diasporafoundation.org/Welcoming_committee) or [pod administrator](https://wiki.diasporafoundation.org/Installation), your help is welcome!

Everyone interacting in diaspora’s codebases, issue trackers, chat rooms, mailing lists, the wiki, and the Loomio group is expected to follow the diaspora\* [code of conduct](/CODE_OF_CONDUCT.md).

## Security

Found a security issue? Please disclose it responsibly. We have a team of developers listening to [security@diasporafoundation.org](mailto:security@diasporafoundation.org). The PGP fingerprint is [AB0D AB02 0FC5 D398 03AB 3CE1 6F70 243F 27AD 886A](https://pgp.mit.edu/pks/lookup?op=get&search=0x6F70243F27AD886A).