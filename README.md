# CLOUDLET: Integrated ownCloud and Diaspora applications #

## ownCloud Server Setup ##

### Docker ###

A software containerization platform called Docker was used to store Ubuntu 14.04 images for the different township servers and global server. Three servers were created for this project, namely two township servers; the Masi server and Ocean View server, and one Global server. 

All the three server images are stored in this Docker link:
[Docker images](https://hub.docker.com/r/bongani/ubuntu/tags/)

In order to install the Ubuntu image servers for a specific location(township or global), you need to [install the Docker Engine](https://docs.docker.com/engine/installation/) on the server computer.

After the docker engine is installed, pull your preferred image with this command.

* Masi server
```
#!ruby

docker pull bongani/ubuntu:masi
```
* Ocean View server
```

#!ruby

docker pull bongani/ubuntu:ocean
```
* Global server 
```
#!ruby

docker pull bongani/ubuntu:global

```

Then create a container for the specific docker images above:

* Masi server
```
#!ruby

docker run -it -p 8081:80 -p 8001:3000 -p 8322:22 --name masiserver bongani/ubuntu:masi /bin/bash
```
* Ocean View server
```
#!ruby


docker run -it -p 8082:80 -p 8002:3000 -p 8322:22 --name oceanserver bongani/ubuntu:ocean /bin/bash

```
* Global server
```
#!ruby

docker run -it -p 8080:80 -p 8000:3000 -p 8322:22 --name globalserver bongani/ubuntu:global /bin/bash
```


### Lampp ###
Each server container has Lampp installed that allows users to run MySQL and HTTP which are required for ownCloud and the synchronization application to work. To start Lampp simply run this command inside each server container:


```
#!ruby

sudo /opt/lampp/lampp restart
```
The localhost HTTP port 80 is mapped to:

* your_server_url:`8081` for Masi server

* your_server_url:`8082` for Ocean View server

* your_server_url:`8080` for Global server


## Demonstration of Synchronization between servers (Bongani) ##
### Rsync ###

Each server container has the `Rsync` and `SSH` installed on the Linux system. The servers have a Linux user called `cloudlet` which initiates the`Rsync` and `SSH` transfer command. The path to the data that is sent is `ownCloud/data/`. This data is sent to the `cloudlet` user of a specific township destination server.

In order for the file transfer to occur, each `cloudlet` and `root` user has to start the `SSH` software package.

Start `SSH` as current user:
```
#!ruby

sudo /etc/init.d/ssh start
```


Log-in as `cloudlet` user:
```
#!ruby

su - cloudlet
password: 1234
```


Start 'SSH' as `cloudlet` user:
```
#!ruby

sudo /etc/init.d/ssh start
```

### Sync Application Branches (MY MAIN CONTRIBUTION) ###
Each township server has a fully functional ownCloud synchronization application installed. This repository has three different add-on ownCloud applications. These applications are stored in three different branches inside this repo. The three applications are my main contribution to the ownCloud environment. To view an application please click a branch link below. 


* [Oceancloudlet branch](https://bitbucket.org/bongani_dube/honours-project-owncloud-synchronization-application/src/08a9c34de1aafecff224ac4b8bba5d71563eb2a5/?at=oceancloudlet)
* [Masicloudlet branch](https://bitbucket.org/bongani_dube/honours-project-owncloud-synchronization-application/src/f7fabc6418f01a78c90ef836ad5e28a3d3811618/?at=masicloudlet)
* [Globalcloudlet branch](https://bitbucket.org/bongani_dube/honours-project-owncloud-synchronization-application/src/36b85ad95442b0a4be6846e3d48cff7fef593768/?at=globalcloudlet)

Each branch represents a server at Ocean view, Masi and Global respectively. And each branch has a README file that will give you more information about the application and how it can be run.

### Cron Scheduler ###
Each township server has a user called `daemon`. This user schedules the cron-tabs that run the synchronization process(`Rsync`). To view the cron-tab at a specific township server, please run this command inside the server containers' terminal:
```
#!ruby

crontab -e
```

### Contact ###

If you need help, please contact me at bdube83@gmail.com.

## Distributed Backup System for ownCloud (Matthew) ##

The Distributed Backup System consists of the following two components:

*	The Distributed Backup Component - ownCloud Server
*	Power User Registration and Backup Notifications Component - ownCloud Android Client.

### ownCloud Server Setup

The above server setup already includes the Distributed Backup System. This Distributed Backup System, however uses its own database to store power user and backup information. The power_users MySQL database dump is included in the `sql/` directory as an exported .sql file. This database has to be hosted on the same server as the owncloud database.

### ownCloud Android Client Setup ###

Make sure you read [android/SETUP.md](https://github.com/owncloud/android/blob/master/SETUP.md) to setup the ownCloud Android application.

### Documentation

For information on extending or maintaining the code base refer to the documentation included with this archive - `globalcloudlet/distributed-backup-docs/site/index.html`.

### Running Tests

#### Distributed Backup (ownCloud Server) Tests

Tests for this project were written using the PHPUnit testing framework and PHPStorm IDE for Linux. These tests are located under the `globalcloudlet/distributed-backup/tests/Tests/DistributedBackTests.php` file. Tests need to be run using the PHPStorm IDE. Alternatively, the PHPUnit framework can be installed on a linux machine. See the [PHPUnit Getting Started](https://phpunit.de/getting-started.html) guide for further information.

#### Power User Registration and Backup (ownCloud Android) Tests

The tests for the power users registration and backup process are included under the `android/powerusersbackup-tests/` directory. The tests for this project are built using maven. Maven is needed to run tests. 

To install Maven:
1. Download maven - https://maven.apache.org/download.cgi
2. Follow the installation instructions - https://maven.apache.org/install.html

Dependencies: JUnit

### Compile, run unit tests and package project into a jar file:

	mvn package

### Run unit tests:

	mvn test

## Diaspora-ownCloud Integration Setup (Takunda) ##

The setup for the plugin to integrate to ownCloud has been document and can be found [here](https://bitbucket.org/Takuchirema/diaspora). This contains the most recent version of the plugin, please refer to the repository for the latest plugin. This documentation gives more information about the server setup.

### Documentation

The main repository with the updated code for this project can be accessed [here](https://bitbucket.org/Takuchirema/diaspora).

### Diaspora Server Setup

Diaspora is running on the docker containers of Masi and Ocean View. 

```
#!ruby

bongani/ubuntu:masi
bongani/ubuntu:ocean
```
It uses the same Lampp application used by ownCloud for creating a MySQL database. The example database.yml is given [here](https://bitbucket.org/Takuchirema/diaspora/src/71ba804bcf937f55d6449218d811435bf2439f00/config/?at=develop). It shows how the database was set up. Notice the line:

```
#!ruby
socket: /opt/lampp/var/mysql/mysql.sock
```

The socket being used for MySQL is the one created by Lampp when it is started. For diaspora.yml under server, the environment must be production.

```
#!ruby
rails_environment: 'production'
```

These settings are set on the installation Diaspora itself as shown [here](https://wiki.diasporafoundation.org/Installation/Ubuntu/Precise).

### RSpec Tests

To run RSpec tests, the files are found in the folder diaspora_owncloud/rspec. To run a single test insert this line in the root folder i.e. in Diaspora:

```
#!ruby
sudo bin/rspec diaspora_owncloud/rspec/controllers/diaspora_owncloud_home_controller_spec.rb
```

If there is a problem with spring (gem for optimizing tests), it can be removed by the line below:


```
#!ruby
bin/spring binstub --remove --all
```

More information about running RSpec tests for Diaspora can be found [here](https://wiki.diasporafoundation.org/Testing_workflow).