# Power Users API reference

This is the reference guide for using the API to create, read, update, delete power user and backup details. 
The API is written PHP and is hosted as a single PHP file with the owncloud server.

## How to use the API

Perform CRUD (Create, Read, Update, Delete) operations using the api with the following url format:

```
[HTTP_REQUEST] http://{owncloud_server_url}/api.php/{endpoint}
```

### PHP Example

An example of how to use the API in PHP using curl:

```
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, "http://{owncloud_server_url}/api.php/backup_file/2016-10-27-17-05-19Matthew");
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
curl_setopt($ch, CURLOPT_POSTFIELDS, "status=Restored");
$result = curl_exec($ch);
curl_close($ch);
```

### Java Example

An example of how to use the API in Java:

```
URL url = new URL("http://{owncloud_server_url}/api.php/users");
HttpURLConnection conn = (HttpURLConnection) url.openConnection();
conn.setReadTimeout(READ_TIMEOUT);
conn.setConnectTimeout(CONNECTION_TIMEOUT);
conn.setRequestMethod("GET");
conn.connect();
```

## Usage

You can do all CRUD (Create, Read, Update, Delete) operations and one extra List operation. Here is how:

### List Power Users

```
GET http://{owncloud_server_url}/api.php/users
```

Output:

```
{"users":{"columns":["uid","password"],"records":[["John","1234"],["Matthew","1234"],["Peter","1234"]]}}
```

### Get Power User

```
GET http://{owncloud_server_url}/api.php/users/uid
```

Example:

```
GET http://{owncloud_server_url}/api.php/users/Matthew
```

Output:

```
{"uid":"Matthew","password":"1234"}
```

### Create New Power User

```
POST http://{owncloud_server_url}/api.php/users
```

```
"uid=Luke&password=1234"
```

Output:

```
[Last insert row ID]
```

### Delete Power User

```
DELETE http://{owncloud_server_url}/api.php/users/uid
```

Example:

```
DELETE http://{owncloud_server_url}/api.php/users/Matthew
```

Output:

```
[Row Affected]
```

### List Backup Log

```
GET http://{owncloud_server_url}/api.php/backup_history
```

Output:

```
{"backup_history":{"columns":["timestamp","num_of_parts","total_size","status"],"records":[["2016-10-05-22-20-41","3","12375600","Restored"]]}}
```

### Get Specific Backup Details

```
GET http://{owncloud_server_url}/api.php/backup_history/timestamp
```

Example:

```
GET http://{owncloud_server_url}/api.php/backup_history/2016-10-05-22-20-41
```

Output:

```
{"timestamp":"2016-10-05-22-20-41","num_of_parts":"3","total_size":"12375600","status":"Restored"}
```

### Create New Backup

```
POST http://{owncloud_server_url}/api.php/backup_history
```

```
"timestamp=2016-10-05-22-20-41&num_of_parts=3&total_size=12375600&status=Restored"
```

Output:

```
[Last insert row ID]
```

### Update Backup Details

```
PUT http://{owncloud_server_url}/api.php/backup_history/timestamp
```

Example:

```
PUT http://{owncloud_server_url}/api.php/backup_history/2016-10-05-22-20-41
```

```
"status=Restored"
```

Output:

```
[Row affected]
```

### Delete Backup Details

```
DELETE http://{owncloud_server_url}/api.php/backup_history/timestamp
```

Example:

```
DELETE http://{owncloud_server_url}/api.php/backup_history/2016-10-05-22-20-41
```

Output:

```
[Row Affected]
```

### List Backup Files

```
GET http://{owncloud_server_url}/api.php/backup_file
```

Output:

```
{"backup_file":{"columns":["id","timestamp","status","uid","filename","filesize","file_location"],"records":[["2016-10-05-22-20-41John","2016-10-05-22-20-41","Restored","John","backup.zip.0","4125210",""]]}}
```

### Get Specific Backup File

```
GET http://{owncloud_server_url}/api.php/backup_file/id
```

Example:

```
GET http://{owncloud_server_url}/api.php/backup_file/2016-10-05-22-20-41Matthew
```

Output:

```
{"id":"2016-10-05-22-20-41Matthew","timestamp":"2016-10-05-22-20-41","status":"Restored","uid":"Matthew","filename":"backup.zip.1","filesize":"4125210","file_location":""}
```

### Create New Backup File

```
POST http://{owncloud_server_url}/api.php/backup_file
```

```
"id=2016-10-05-22-20-41Matthew&timestamp=2016-10-05-22-20-41&status=Restored&uid=Matthew&filename=backup.zip.1&filesize=4125210"
```

Output:

```
[Last insert row ID]
```

### Update Backup File

```
PUT http://{owncloud_server_url}/api.php/backup_file/id
```

Example:

```
PUT http://{owncloud_server_url}/api.php/backup_file/2016-10-05-22-20-41Matthew
```

```
"status=Restored"
```

Output:

```
[Row affected]
```

### Delete Backup File

```
DELETE http://{owncloud_server_url}/api.php/backup_file/id
```

Example:

```
DELETE http://{owncloud_server_url}/api.php/backup_file/2016-10-05-22-20-41Matthew
```

Output:

```
[Row Affected]
```

