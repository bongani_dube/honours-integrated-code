# 1. Getting Started

This guide describes the application code setup and how to maintain and/or extend the application.
The Distributed Backup System consists of the following two components:

*	The Distributed Backup Component
*	Power User Registration and Backup Notifications Component.

## 1.1 Distributed Backup

This component is implemented on the ownCloud server application.
The relevant source files are in the following directories:

*	**`distributed-backup/`**
	*	`DistributedBackup.php` - PHP file that contains code for handling the backup of the ownCloud file system. Executed upon click of the 'Execute Backup' button defined in admin.php.
	*	`RestoreBackup.php` - PHP file that contains code for handling the restore of the ownCloud file system from backup. Executed upon click of the 'Restore Backup' button defined in admin.php.
	*	`DistributedBackupConfig.php` - Configuration file for specifying backup directories, API urls and MySQL database parameters.
	*	`MultipartCompress.php` - Class that handles zip compression of files.
	*	`FileAccess.php` - Class that contains methods for accessing ownCloud files (Upload, Download, Delete) using the WebDav API.
	*	**`tests/`**
		*	**`Tests/`**
			*	`DistributedBackupTests.php` - PHP file for writing PHPUnit Tests.
*	**`settings/`**
	*	**`templates/`**
		*	`admin.php` - PHP file that contains HTML code responsible for the Distributed Backup User Interface.
	*	**`js/`**
		*	`admin.js` - File that contains Javascript code that handles input events of HTML components specified in admin.php.
*	`api.php` - PHP Power Users API 

### 1.1.1 Modifying the Backup and Restore Process

The backup and restore processes are handled by the **DistributedBackup.php** and **RestoreBackup.php** files respectively. Any modifications to be made to these process should be done in these php files.

Changes to the FileAccess and/or MultipartCompress classes should be done in the **FileAccess.php** and **MultipartCompress.php** files.
New functionality that does not fit into these classes should be created as a new class and included in the appropriate php files.

Example:

```
// New class NewFeature.php has been created with methods for new functionality
include("NewFeature.php");
...
// Call methods
NewFeature::methodName(params);
...
```

### 1.1.2 Modifying the User Interface

Modifications to the Distributed Backup user interface are to be made to the **admin.php** and **admin.js** files. 

#### Changing the View - HTML/PHP

The user interface code is located under the *distributed-backup* html section in **admin.php**:

```
...
<div class="section" id="distributed-backup">

	// UI code here

</div>
...
```

This user interface is found under the *admin* page (only available to admin users)  of the ownCloud web application.

#### Event handling for HTML input components - Javascript

Events for input components should be implemented as ajax requests as represented below:

Example:

```
...

$('#executeBackupButton').click(function(){
		
	$.ajax({
	url:'../../distributed-backup/DistributedBackup.php',
	type: 'GET',
	success:function(data){	
		alert(data);
	}
	});

});

$('#restoreBackupButton').click(function(){
	
	$.ajax({
	url:'../../distributed-backup/RestoreBackup.php',
	type: 'GET',
	success:function(data){	
		alert(data);
	}
	});

});

...

```

### 1.1.3 Writing Tests

Tests should written as PHPUnit tests in the **DistributedBackupTests.php** file. For a more detailed guide on writing PHPUnit tests see [Writing Tests for PHPUnit](https://phpunit.de/manual/current/en/writing-tests-for-phpunit.html).

Example:

```
...

public function testNewFunctionality(){
	//Test code here
}

...

```

## 1.2 Power User Registration and Backup Notifications

This component is implemented on the ownCloud Android client application.
The relevant source files are in the following directories:

*	**`src/`**
	*	**`com.owncloud.android/`**
		*	**`powerusersbackup/`**
			*	**`activity/`**
				*	`PowerUserActivity.java` - Android Activity for handling Power User sign up and cancellation. Layout of the activity specified in **activity_power_user.xml**.
			*	**`asynctasks/`**
				*	`CancelPowerUserAsyncTask.java` - Background task for handling cancellation of Power User registration by communicating with Power Users API.
				*	`CheckPowerUserAsyncTask.java` - Background task for checking whether a user is a Power User
				*	`NotificationAsyncTask.java` - Background task for handling notifications when the status of backup files change.
				*	`SignUpPowerUserAsyncTask.java` - Background tasks for creating new Power Users.
				*	`UpdateBackupFileLocationAsyncTask.java` - Background task for changing the file location of a backup file upon status change to *Downloaded*.
				*	`UpdateBackupStatusAsyncTask.java` - Background task for changing the status of a backup file.
			*	**`fragment/`**
				*	`PowerUserCancelFragment.java` - Fragment class managed by **PowerUserActivity.java**. The layout of the fragment is specified in the **fragment_power_user_cancel.xml** file.
				*	`PowerUserSignUpFragment.java` - Fragment class managed by **PowerUserActivity.java**. The layout of the fragment is specified in the **fragment_power_user_signup.xml** file.
		*	**`ui/`**
			*	**`activity/`**
				*	`FileActivity.java` - Activity responsible for navigation menu items of the ownCloud Android application.
				*	`FileDisplayActivity.java` - The Main Activity of the ownCloud Android application. Background tasks and notifications are executed from here.
*	**`res/`**
	*	**`layout/`**
		*	`activity_power_user.xml` - Layout of **PowerUserActivity.java**
		*	`fragment_power_user_cancel.xml` - Layout of **PowerUserCancelFragment.java**
		*	`fragment_power_user_signup.xml` - Layout of **PowerUserSignUpFragment.java**

### Power User Sign Up/Cancel User Interface

Changes to the power user sign and cancel user interface are to be made to the **PowerUserActivity**, which makes use of the **PowerUserSignUpFragment** and **PowerUserCancelFragment** fragments. 
See the following links for more details on Android [Activities](https://developer.android.com/guide/components/activities.html) and [Fragments](https://developer.android.com/guide/components/fragments.html).

### AsyncTasks that communicates with the Power Users API

Background Tasks are written as classes that extend the AsyncTask<> class. 
An example of a class that extends AsyncTask<>:

```

public class BackgroundAsyncTask extends AsyncTask<String, String, String> {

...

	@Override
    protected String doInBackground(String... params) {
    	// Code that executes in background goes here.
    }

    @Override
    protected void onPostExecute(String result) {
        // Code that executes upon completion of doInBackground goes here.
    }

...

}

```

Have a look at the other AsyncTasks for an idea on how to connect and send HTTP requests to the Power Users API.

### Backup Notifications 

Backup notifications are implemented as Android Dialogs in the **FileDisplayActivity** class. The existing **displayBackupRequestDialog()** and **displayRestoreRequestDialog()** handle the display of the backup and restore notifications respectively.

These notifications are displayed upon completion of the corresponding async tasks.
