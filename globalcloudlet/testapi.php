<?php
/**
 * Created by PhpStorm.
 * User: matthew
 * Date: 2016/09/27
 * Time: 7:32 PM
 */

//API tests

/*$jsonString = file_get_contents("http://localhost/owncloud/api.php/users");
$data = json_decode($jsonString);

$arr = $data->users->records;

echo count($arr);

foreach ($arr as $row){
    $uid = $row[0];
    $password = $row[1];

    echo $uid.":".$password;
}*/

//CURL test

//Test URL connection

$URL = "http://localhost/owncloud/remote.php/webdav/";
$username = "Matthew";
$password = "1234";
$filename = "robots.txt";

$ch = curl_init($URL);
curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
curl_setopt($ch, CURLOPT_NOBODY, 1);
$result = curl_exec($ch);
$httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
curl_close($ch);

if($httpCode == 200){   //Success

    //Check if file to be sent exists
    if(file_exists($filename)){

        try {
            $dest_URL = "http://localhost/owncloud/remote.php/webdav/$filename";

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $dest_URL);
            curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
            curl_setopt($ch, CURLOPT_PUT, 1);

            $file = fopen($filename, 'r');

            curl_setopt($ch, CURLOPT_INFILE, $file);
            curl_setopt($ch, CURLOPT_INFILESIZE, filesize($filename));

            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_BINARYTRANSFER, true);

            curl_exec($ch);
            //echo $curl_response_res;
            //$status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            //echo $status."\n";

            fclose($file);
            curl_close($ch);

        } catch(Exception $e){
            echo $e->getMessage();
        }

    }
    else{
        echo "File $filename does not exist";
    }
}
else if($httpCode == 401){
    echo "401 Login Failure.\n";
}
else if($httpCode == 400){
    echo "400 Invalid Request.\n";
}
else if($httpCode == 404){
    echo "404 Not Found.\n";
}
else if($httpCode == 500){
    echo "500 Internal Server Error\n";
}
else if($httpCode == 502){
    echo "502 Servers may be down\n";
}
else if($httpCode == 503){
    echo "503 Servers unavailable\n";
}

/*try {
    $username = "Matthew";
    $password = "1234";
    $filename = "api.php";
    $URL = "http://localhost/owncloud/remote.php/webdav/$filename";

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $URL);
    curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
    curl_setopt($ch, CURLOPT_PUT, 1);

    $file = fopen($filename, 'r');

    curl_setopt($ch, CURLOPT_INFILE, $file);
    curl_setopt($ch, CURLOPT_INFILESIZE, filesize($filename));

    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_HEADER, 1);
    curl_setopt($ch, CURLOPT_NOBODY, 1);
    curl_setopt($ch, CURLOPT_BINARYTRANSFER, true);

    curl_exec($ch);
    //echo $curl_response_res;
    //$status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    //echo $status."\n";

    fclose($file);
    curl_close($ch);

} catch(Exception $e){
    echo $e->getMessage();
}*/
