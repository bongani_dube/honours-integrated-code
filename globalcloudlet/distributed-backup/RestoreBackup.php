<?php

include("MultipartCompress.php");
include("DistributedBackupConfig.php");
include("FileAccess.php");
include("Utils.php");

// Get number of power users from the users table in the owncloud db

$jsonString = file_get_contents($api_url."/users");
$data = json_decode($jsonString);

$powerUsers = $data->users->records;
$numPowerUsers = count($powerUsers);
echo "Power Users: ".$numPowerUsers."\n";

//Get last backup timestamp

$jsonString = file_get_contents($api_url."/backup_history?order=timestamp,desc");
$data = json_decode($jsonString);

$backups = $data->backup_history->records;

$lastBackup = "";
foreach ($backups as $row){
	$lastBackup = $row[0];
	break;
}

$date = $lastBackup;
echo "Selected last backup: ".$date."\n";


if($numPowerUsers > 0){

	// Check if backup parts are uploaded to power user accounts before attempting to restore

	$index = 0;
	$all_parts_exist = true;
	foreach ($powerUsers as $row) {
		$uid = $row[0];
		$password = $row[1];

		$filename = "" . $target . "." . $index;

		$exists = FileAccess::fileExists($webdav_url, $filename, $uid, $password);
		if(!$exists){
			$all_parts_exist = false;
			//break;
		}
		$index++;
	}

	if($all_parts_exist) {

		// Download parts from power user devices

		try {
			// output data of each row
			$index = 0;
			foreach ($powerUsers as $row) {
				$uid = $row[0];
				$password = $row[1];

				$filename = "" . $target . "." . $index;

				//mkdir($date);
				//exec("curl -u $uid:$password $webdav_url".$date."/".$filename." --output ".$filename);
				//exec("curl -u $uid:$password $webdav_url".$filename." --output ".$filename);
				$download = FileAccess::downloadFile($webdav_url, $filename, $uid, $password);
				if ($download) {
					//Remove uploaded file from user once downloaded
					$deleted = FileAccess::deleteFile($webdav_url, $filename, $uid, $password);
				}
				$index++;
			}

			echo "Download of backup parts completed.\n";

		} catch (Exception $e) {
			echo "Download of backup parts failed.\n";
		}

		// Merge and unzip

		$merge = MultipartCompress::merge($target, $numPowerUsers);
		if ($merge) {
			echo "Merge Successful\n";
			// Remove parts
			for ($i = 0; $i < $numPowerUsers; ++$i) {
				unlink("$target.$i");
			}
		} else {
			echo "Merge Failed\n";
		}

		$fileSize = filesize($target);

		$unzip = MultipartCompress::unzip($target, $backup_dir);
		if ($unzip) {
			echo "Decompression Successful\n";
		} else {
			echo "Decompression Failed\n";
		}

		if ($merge and $unzip) {

			// Log restore details in db

			$new_status = "Restored";
			$post_string = "status=$new_status";

			try {

				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, $api_url . "/backup_history/$date");
				curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
				curl_setopt($ch, CURLOPT_POSTFIELDS, $post_string);

				$result = curl_exec($ch);
				curl_close($ch);
			} catch(Exception $e){
				echo "Update backup history status failed.\n";
			}

			echo "Restore details stored in restore history.\n";

			// Update backup status in backup_file table

			try {

				foreach ($powerUsers as $row) {

					$uid = $row[0];

					$ch = curl_init();
					curl_setopt($ch, CURLOPT_URL, $api_url . "/backup_file/$date$uid");
					curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
					curl_setopt($ch, CURLOPT_POSTFIELDS, $post_string);

					$result = curl_exec($ch);
					curl_close($ch);
				}
			} catch(Exception $e){
				echo "Update backup file status failed.\n";
			}

			// Restore existing data/ dir (rsync) and restore db

			exec("rsync -Aax $backup_dir $dir");  //restore /data dir
			exec("$mysql_install_loc -h $db_host -u$db_username -p$db_password $db_name < $backup_dir/$db_backupfile");   //restore owncloud database

			// Remove backup dir after restore

			// Utils::deleteDir($backup_dir);
		}


	} else{
		echo "All backup parts are not available on power user accounts.\n";


		// Check if backup status for each user is downloaded
		// Change request to waiting for upload

		try {
			foreach ($powerUsers as $row) {
				$uid = $row[0];

				$jsonString = file_get_contents($api_url."/backup_file/$date$uid");
				$data = json_decode($jsonString);

				$status = $data->status;

				if($status == "Downloaded"){

					$new_status = "Waiting";
					$post_string = "status=$new_status";

					$ch = curl_init();
					curl_setopt($ch, CURLOPT_URL, $api_url . "/backup_file/$date$uid");
					curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
					curl_setopt($ch, CURLOPT_POSTFIELDS, $post_string);

					$result = curl_exec($ch);
					curl_close($ch);
				}
			}

			echo "Sent requests to power users to upload backup parts for restore.\n";

		} catch(Exception $e){
			echo "Update backup file status failed.\n";
		}
	}

} else {
	echo "No power users added.\n";
}

?>
