<?php

/**
 * Created by PhpStorm.
 * User: wllmat011
 * Date: 2016/09/29
 * Time: 11:58 AM
 */

class SimpleTest extends PHPUnit_Framework_TestCase
{

    public function testDBApiCRUD()
    {
        // Test if CRUD methods for api endpoints
        $api_url = "http://localhost/owncloud/powerusers-api.php";

        // List and Order records
        // ----------------------

        $jsonString = file_get_contents($api_url . "/users");
        $this->assertJson($jsonString);

        $jsonString = file_get_contents($api_url . "/backup_file?order=timestamp,desc");
        $this->assertJson($jsonString);

        $jsonString = file_get_contents($api_url . "/backup_history?order=timestamp");
        $this->assertJson($jsonString);

        // CREATE record
        // --------------

        $date = "2016-09-29-08-22-52";
        $numOfParts = 2;
        $totalSize = 123456;
        $status = "Uploaded";

        $post_string = "timestamp=$date&num_of_parts=$numOfParts&total_size=$totalSize&status=$status";

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $api_url."/backup_history");
        curl_setopt($ch, CURLOPT_POST, 3);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_string);
        $result = curl_exec($ch);
        curl_close($ch);

        // Check if Inserted

        $jsonString = file_get_contents($api_url . "/backup_history/$date");
        $this->assertJson($jsonString);


        // UPDATE record
        // -------------

        $newSize = 654321;
        $post_string = "timestamp=$date&num_of_parts=$numOfParts&total_size=$newSize&status=$status";

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $api_url."/backup_history/$date");
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_string);

        $result = curl_exec($ch);
        curl_close($ch);

        //Check if Updated
        $jsonString = file_get_contents($api_url . "/backup_history/$date");
        $data = json_decode($jsonString);
        $totalSize = $data->total_size;

        $this->assertEquals($totalSize, $newSize);

        // DELETE record
        // -------------

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $api_url."/backup_history/$date");
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
        $result = curl_exec($ch);
        curl_close($ch);

        // Check if Deleted
        $jsonString = file_get_contents($api_url . "/backup_history");
        $data = json_decode($jsonString);
        $restores = $data->backup_history->records;
        $deleted = true;
        foreach ($restores as $row){
            if($row[0] == $date){
                $deleted = false;
                break;
            }
        }
        $this->assertTrue($deleted);

    }

    public function testWebDavInteractions(){

        // TEST WebDav API Connection
        // --------------------------

        $url = "http://localhost/owncloud/remote.php/webdav/";
        $username = "John";
        $password = "1234";

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
        curl_setopt($ch, CURLOPT_NOBODY, 1);
        curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        $this->assertEquals($httpCode, 200);

        // UPLOAD file to ownCloud user
        // ----------------------------

        $filename = "file.txt";
        $dest_URL = "http://localhost/owncloud/remote.php/webdav/".$filename;

        // create file
        touch($filename);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $dest_URL);
        curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
        curl_setopt($ch, CURLOPT_PUT, 1);

        $file = fopen($filename, 'r');

        curl_setopt($ch, CURLOPT_INFILE, $file);
        curl_setopt($ch, CURLOPT_INFILESIZE, filesize($filename));

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_BINARYTRANSFER, true);

        curl_exec($ch);

        fclose($file);
        curl_close($ch);

        // Check if file is uploaded

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $dest_URL);
        curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $response = curl_exec($ch);
        curl_close($ch);

        $this->assertEquals($response, '');
        unlink($filename);

        // DOWNLOAD file from ownCloud user
        // ---------------------------------

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $dest_URL);
        curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");

        $file = fopen($filename, 'w');

        curl_setopt($ch, CURLOPT_FILE, $file);

        curl_exec($ch);

        curl_close($ch);
        fclose($file);

        $this->assertFileExists($filename);
        unlink($filename);

        // DELETE file from ownCloud user
        // ------------------------------

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $dest_URL);
        curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
        $response = curl_exec($ch);
        curl_close($ch);

        $this->assertTrue($response);
    }

    public function testMultiPartCompression(){

        // Create temporary data directory with single file
        $dir = "data/";
        mkdir($dir);
        touch($dir."hello.txt");

        $this->assertDirectoryExists($dir);
        $this->assertFileExists($dir."hello.txt");

        // ZIP data directory
        // -------------------

        $target = "backup.zip";

        //Get real path for our folder
        $rootPath = realPath($dir);

        $zp = new ZipArchive();

        if(file_exists($target)){
            $flags = 0;
        }else{
            $flags = ZipArchive::CREATE;
        }

        $zp->open($target, $flags);

        $files = new RecursiveIteratorIterator(
            new RecursiveDirectoryIterator($rootPath),
            RecursiveIteratorIterator::LEAVES_ONLY
        );

        foreach ($files as $name => $file){
            // Skip Directories (they would be added automatically)
            if(!$file->isDir())
            {
                //Get real and relative path for current file
                $filePath = $file->getRealPath();
                $relativePath = substr($filePath, strlen($rootPath) + 1);

                //Add current file to archive
                $zp->addFile($filePath, $relativePath);
            }
        }

        $zp->close();

        $this->assertFileExists($target);

        //Remove direcory and file after zip
        unlink($dir."hello.txt");
        rmdir($dir);

        // SPLIT Zipped file into parts
        // -----------------------------

        $numParts = 2;
        $fileSize = filesize($target);
        $partSize = ceil($fileSize / $numParts);

        $f_in = fopen($target, 'rb');
        for ($i = 0; $i < $numParts; ++$i) {
            $f_out = fopen("$target.$i", 'wb');
            stream_copy_to_stream($f_in, $f_out, $partSize);
            fclose($f_out);
        }
        fclose($f_in);

        for ($i = 0; $i < $numParts; ++$i) {
            $this->assertFileExists("$target.$i");
        }
        unlink($target);    //remove zip after split

        // MERGE split zipped parts
        // -------------------------

        $content = "";

        for ($i = 0; $i < $numParts; ++$i) {
            $f_in = fopen("$target.$i", 'rb');
            $partSize = filesize("$target.$i");
            //stream_copy_to_stream($f_out, $f_in, $partSize);

            $content .= fread($f_in, $partSize);
            fclose($f_in);
        }

        $f_out = fopen($target, 'wb');

        fwrite($f_out, $content);
        fclose($f_out);

        //Remove parts after merge
        for ($i = 0; $i < $numParts; ++$i) {
            unlink("$target.$i");
        }

        $this->assertFileExists($target);

        // UNZIP merge file
        // -----------------

        $output = "output/";

        $zp = new ZipArchive();
        $zp->open($target);
        $zp->extractTo($output);
        $zp->close();
        unset($zp);

        $this->assertDirectoryExists($output);
        $this->assertFileExists($output."hello.txt");

        unlink($target);
        unlink($output."hello.txt");
        rmdir($output);
    }

    /*public function testEncryption(){

        $filename = "helloworld.txt";
        $passphrase = "My secret";

        //Convert passphrase into iv/key pair

        $iv = substr(md5("\x1B\x3C\x58".$passphrase, true), 0, 8);
        $key = substr(md5("\x2D\xFC\xD8".$passphrase, true) .
            md5("\x2D\xFC\xD9".$passphrase, true), 0, 24);
        $opts = array(
            'iv' => $iv,
            'key' => $key,
            'mode' => 'stream'
        );

        //open file
        $fp = fopen($filename, 'wb');

        // Add Mcrypt stream filter - Triple DES
        stream_filter_append($fp, 'mcrypt.tripledes', STREAM_FILTER_WRITE, $opts);

        fwrite($fp, "Secret secret secret data");

        fclose($fp);

        //Convert passphrase into iv/key pair

        $iv = substr(md5("\x1B\x3C\x58".$passphrase, true), 0, 8);
        $key = substr(md5("\x2D\xFC\xD8".$passphrase, true) .
            md5("\x2D\xFC\xD9".$passphrase, true), 0, 24);
        $opts = array(
            'iv' => $iv,
            'key' => $key,
            'mode' => 'stream'
        );

        //open file
        $fp = fopen($filename, 'rb');

        // Add Mcrypt stream filter - Triple DES
        stream_filter_append($fp, 'mdecrypt.tripledes', STREAM_FILTER_READ, $opts);

        fpassthru($content);

    }*/

}
