<?php

class MultipartCompress{
    
    /**
     * Compact a file in multipart zip archive.
     * @param string $i The file to compact.
     * @param string $o The zip archive (*.zip).
     * @param integer $s The mnax size (in byte) for the parts. 0 to no parts.
     * @return boolean Return number of parts created.
     */
    public static function zip($i, $o, $s = 0){
        
	    //Get real path for our folder
	    $rootPath = realPath($i);

        $zp = new ZipArchive();
        
        if(file_exists($o)){
            $flags = 0;
        }else{
            $flags = ZipArchive::CREATE;
        }
        
        $zp->open($o, $flags);
        
	    $files = new RecursiveIteratorIterator(
	        new RecursiveDirectoryIterator($rootPath),
		    RecursiveIteratorIterator::LEAVES_ONLY
        );

        foreach ($files as $name => $file){
            // Skip Directories (they would be added automatically)
            if(!$file->isDir())
            {
                //Get real and relative path for current file
                $filePath = $file->getRealPath();
                $relativePath = substr($filePath, strlen($rootPath) + 1);

                //Add current file to archive
                $zp->addFile($filePath, $relativePath);
            }
        }

        $zp->close();
            
    }

     /**
     * Split the zip archive.
     * @param string $fileName The zip archive.
     * @param integer $partSize The max size for the parts.
     * @return integer Return the number of parts created.
     */
    public static function split($fileName, $partSize, $numPowerUsers){

        $fileSize = filesize($fileName);

        try{

            $f_in = fopen($fileName, 'rb');
            for ($i = 0; $i < $numPowerUsers; ++$i) {
                $f_out = fopen("$fileName.$i", 'wb');
                stream_copy_to_stream($f_in, $f_out, $partSize);
                fclose($f_out);
            }
            fclose($f_in);

            return true;
        }
        catch(Exception $e){
            echo 'Message: ' .$e->getMessage();
            return false;
        }
    }
    
    /**
     * Decompact the zip archive.
     * @param string $i The zip archive (*.zip).
     * @param string $o The directory name for extract.
     * @param integer $p Number of parts of the zip archive.
     * @return boolean Return TRUE for success or FALSE for fail.
     */
    public static function unzip($i, $o, $p = 0){
        
        $zp = new ZipArchive();
        $zp->open($i);
        if($zp->extractTo($o)){
            $zp->close();
            unset($zp);
            unlink($i);
            return true;
        }else{
            return false;
        }
        
    }
    
    /**
     * Merge the parts of zip archive.
     * @param string $i The zip archive (*.zip).
     * @param integer $p Number of parts of the zip archive.
     * @return boolean Return TRUE for success or FALSE for fail.
     */
    public static function merge($fileName, $numParts){

        try{

            $content = "";

            for ($i = 0; $i < $numParts; ++$i) {
                $f_in = fopen("$fileName.$i", 'rb');
                $partSize = filesize("$fileName.$i");
                //stream_copy_to_stream($f_out, $f_in, $partSize);

                $content .= fread($f_in, $partSize);
                fclose($f_in);
            }

            $f_out = fopen($fileName, 'wb');

            fwrite($f_out, $content);
            fclose($f_out);

            return true;
        }
        catch(Exception $e){
            echo 'Message: ' .$e->getMessage();
            return false;
        }
    }

    public static function encryptFile($filename, $passphrase){

        //Convert passphrase into iv/key pair

        $iv = substr(md5("\x1B\x3C\x58".$passphrase, true), 0, 8);
        $key = substr(md5("\x2D\xFC\xD8".$passphrase, true) .
            md5("\x2D\xFC\xD9".$passphrase, true), 0, 24);
        $opts = array(
            'iv' => $iv,
            'key' => $key,
            'mode' => 'stream'
        );

        //open file
        $fp = fopen($filename, 'wb');

        // Add Mcrypt stream filter - Triple DES
        stream_filter_append($fp, 'mcrypt.tripledes', STREAM_FILTER_WRITE, $opts);

        fclose($fp);
    }

    public static function decryptFile($filename, $passphrase){

        $content = "";

        //Convert passphrase into iv/key pair

        $iv = substr(md5("\x1B\x3C\x58".$passphrase, true), 0, 8);
        $key = substr(md5("\x2D\xFC\xD8".$passphrase, true) .
            md5("\x2D\xFC\xD9".$passphrase, true), 0, 24);
        $opts = array(
            'iv' => $iv,
            'key' => $key,
            'mode' => 'stream'
        );

        //open file
        $fp = fopen($filename, 'rb');

        // Add Mcrypt stream filter - Triple DES
        stream_filter_append($fp, 'mdecrypt.tripledes', STREAM_FILTER_READ, $opts);

        fpassthru($content);

    }

}
