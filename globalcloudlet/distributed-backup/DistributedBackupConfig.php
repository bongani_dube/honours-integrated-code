<?php

// BACKUP directory and archive names
// ----------------------------------

$target = 'backup.zip';
$dir = '../data/';
$db_backupfile = "owncloud-dbbackup.bak";
$backup_dir = "oc-backupdir/";

// API URLs
// --------

$api_url = "http://localhost/globalcloudlet/powerusers-api.php";
$webdav_url = "http://localhost/globalcloudlet/remote.php/webdav/";

// ownCloud Database parameters
// ----------------------------

$db_host = "localhost";
$db_username = "admin";
$db_password = "1234";
$db_name = "owncloud";

// mysql install location
// ----------------------

$mysql_install_loc = "/opt/lampp/bin/mysql";
$mysqldump_install_loc = "/opt/lampp/bin/mysqldump";
