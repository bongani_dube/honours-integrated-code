<?php

/**
 * Created by PhpStorm.
 * User: matthew
 * Date: 2016/09/27
 * Time: 10:51 PM
 */
class FileAccess
{

    protected static function testConnection($url, $username, $password){

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
        curl_setopt($ch, CURLOPT_NOBODY, 1);
        curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        return $httpCode;

    }

    protected static function getHTTPErrorMessage($httpCode){

        if($httpCode == 401){
            return "401 Login Failure.";
        }
        else if($httpCode == 400){
            return "400 Invalid Request.";
        }
        else if($httpCode == 404){
            return "404 Not Found.";
        }
        else if($httpCode == 500){
            return "500 Internal Server Error.";
        }
        else if($httpCode == 502){
            return "502 Servers may be down.";
        }
        else if($httpCode == 503){
            return "503 Servers unavailable.";
        }
        else{
            return "Unidentified HTTP Code.";
        }

    }

    public static function uploadFile($url, $filename, $username, $password){

        //Test URL connection

        $httpCode = self::testConnection($url, $username, $password);

        // Upload file to ownCloud user

        if($httpCode == 200){   //Success

            //Check if file to be sent exists
            if(file_exists($filename)){

                try {
                    $dest_URL = $url.$filename;

                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL, $dest_URL);
                    curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
                    curl_setopt($ch, CURLOPT_PUT, 1);

                    $file = fopen($filename, 'r');

                    curl_setopt($ch, CURLOPT_INFILE, $file);
                    curl_setopt($ch, CURLOPT_INFILESIZE, filesize($filename));

                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($ch, CURLOPT_BINARYTRANSFER, true);

                    curl_exec($ch);
                    //echo $curl_response_res;
                    //$status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                    //echo $status."\n";

                    fclose($file);
                    curl_close($ch);

                } catch(Exception $e){
                    echo $e->getMessage();
                }

                return true;

            }
            else{
                echo "File $filename does not exist";
                return false;
            }
        }
        else {
            echo self::getHTTPErrorMessage($httpCode)."\n";
            return false;
        }

    }

    public static function downloadFile($url, $filename, $username, $password){

        //Test URL connection

        $httpCode = self::testConnection($url, $username, $password);

        // Download file from ownCloud user

        if($httpCode == 200){   //Success

            try {
                $dest_URL = $url.$filename;

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $dest_URL);
                curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");

                $file = fopen($filename, 'w');

                curl_setopt($ch, CURLOPT_FILE, $file);

                curl_exec($ch);
                //echo $curl_response_res;
                //$status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                //echo $status."\n";

                curl_close($ch);
                fclose($file);

            } catch(Exception $e){
                echo $e->getMessage();
                return false;
            }

            return true;

        }
        else {
            echo self::getHTTPErrorMessage($httpCode)."\n";
            return false;
        }

    }

    public static function deleteFile($url, $filename, $username, $password){

        //Test URL connection

        $httpCode = self::testConnection($url, $username, $password);

        if($httpCode == 200) {   //Success

            try {
                $dest_URL = $url . $filename;

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $dest_URL);
                curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
                $response = curl_exec($ch);
                curl_close($ch);

            } catch(Exception $e){
                echo $e->getMessage();
                return false;
            }

            return true;

        }
        else {
            echo self::getHTTPErrorMessage($httpCode)."\n";
            return false;
        }
    }

    public static function fileExists($url, $filename, $username, $password){

        try {
            $dest_URL = $url . $filename;

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $dest_URL);
            curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
            curl_setopt($ch, CURLOPT_NOBODY, true);
            //curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            $response = curl_exec($ch);
            $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            curl_close($ch);

            if($httpCode == 200) {
                return true;
            }
            else{
                //echo $response."\n";
                return false;
            }

        } catch(Exception $e){
            echo $e->getMessage()."\n";
            //echo "$filename does not exist on $username's account.\n";
            return false;
        }
    }

}