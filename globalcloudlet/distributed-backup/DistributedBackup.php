<?php

//Get the number of power users, compress and split data/ directory into [num_power_users] parts zip archive

include("MultipartCompress.php");
include("DistributedBackupConfig.php");
include("FileAccess.php");

// Get number of power users from the users table in the owncloud db

$jsonString = file_get_contents($api_url."/users");
$data = json_decode($jsonString);

$powerUsers = $data->users->records;
$numPowerUsers = count($powerUsers);
echo "Power Users: ".$numPowerUsers."\n";

try {
	if ($numPowerUsers > 0) {

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

		//Create db backup
		exec("$mysqldump_install_loc -h $db_host -u$db_username -p$db_password $db_name > $db_backupfile");
		if(file_exists($db_backupfile)) {

			rename($db_backupfile, "$dir/$db_backupfile");

			$archive = MultipartCompress::zip($dir, $target);

			$fileSize = filesize($target);
			echo "Archive file size: " . $fileSize . "\n";
			$partSize = ceil($fileSize / $numPowerUsers);
			echo "Parts Size: " . $partSize . "\n";

			$split = MultipartCompress::split($target, $partSize, $numPowerUsers);
			if ($split) {
				echo "Splitting archive successful\n";
				// Remove backup.zip
				unlink($target);
			}

			// Get power users and upload parts to power users using curl webdav
			$date = date("Y-m-d-H-i-s");

			// output data of each row
			$index = 0;
			foreach ($powerUsers as $row) {
				$uid = $row[0];
				$password = $row[1];

				$filename = "" . $target . "." . $index;
				chmod($filename, 777);

				// create backup folder with current date/time
				//exec("curl -u $uid:$password -X MKCOL $web_url" . $date);

				//exec("curl -u $uid:$password -T $filename $web_url" . $date . "/" . $filename);
				$upload = FileAccess::uploadFile($webdav_url, $filename, $uid, $password);

				//Log in backup_file table in db

				$postdata = http_build_query(
					array(
						'id' => $date . $uid,
						'timestamp' => $date,
						'status' => 'Uploaded',
						'uid' => $uid,
						'filename' => $filename,
						'filesize' => $partSize
					)
				);

				$opts = array('http' =>
					array(
						'method' => 'POST',
						'header' => 'Content-type: application/x-www-form-urlencoded',
						'content' => $postdata
					)
				);

				$context = stream_context_create($opts);
				$result = file_get_contents($api_url . "/backup_file", false, $context);

				echo "Backup file details for $uid stored in backup files table.\n";

				// Remove backup.zip parts after upload
				unlink($filename);

				$index++;
			}

			echo "Upload to Power Users completed.\n";

			// Log in backup_history table in db

			$postdata = http_build_query(
				array(
					'timestamp' => $date,
					'num_of_parts' => $numPowerUsers,
					'total_size' => $fileSize,
					'status' => 'Uploaded'
				)
			);

			$opts = array('http' =>
				array(
					'method' => 'POST',
					'header' => 'Content-type: application/x-www-form-urlencoded',
					'content' => $postdata
				)
			);

			$context = stream_context_create($opts);
			$result = file_get_contents($api_url . "/backup_history", false, $context);

			echo "Backup details stored in backup history.\n";

		} else{
			echo "Database backup could not be created.\n";
		}

	} else {
		echo "No power users added.\n";
	}
} catch(Exception $e){
	echo $e->getMessage();
}

?>
